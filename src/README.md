# okteto-hello-world

## Setup venv

```
python3 -m venv venv
```

## Use venv

```
source venv/bin/activate
```

## Install packages

```
pip3 -r requirements.txt
```
